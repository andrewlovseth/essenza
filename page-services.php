<?php get_header(); ?>

	<section id="page-header">
		<div class="wrapper">
		
			<div class="info">
				<h3><?php the_title(); ?></h3>
				<p><?php the_field('deck'); ?></p>
			</div>
			
		</div>
	</section>


	<?php if(have_rows('services')): while(have_rows('services')) : the_row(); ?>
	 
	    <?php if( get_row_layout() == 'photo' ): ?>
			
			<section class="photo backstretch" data-img-src="<?php $image = get_sub_field('image'); echo $image['url']; ?>">
	
			</section>
			
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'basic' ): ?>
			
			<section class="services-info basic">
				<div class="wrapper">
					
					<h2><?php the_sub_field('headline'); ?></h2>
					<p class="deck"><?php the_sub_field('deck'); ?></p>
					
					<?php if(get_sub_field('cta_mail')): ?>
						<a href="mailto:<?php the_sub_field('cta_mail'); ?>?subject=<?php the_sub_field('cta_label'); ?>" class="cta"><?php the_sub_field('cta_label'); ?></a>
					<?php endif; ?>
					<?php if(get_sub_field('cta_link')): ?>
						<a href="<?php the_sub_field('cta_link'); ?>" class="cta"><?php the_sub_field('cta_label'); ?></a>		
					<?php endif; ?>
				
				</div>
			</section>
			
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'extended' ): ?>
			
			<section class="services-info extended">
				<div class="wrapper">
				
					<h2><?php the_sub_field('headline'); ?></h2>
					<p class="deck"><?php the_sub_field('deck'); ?></p>
					
					<?php if(get_sub_field('features')): while(has_sub_field('features')): ?>
					
						<div class="feature">
							<h3><?php the_sub_field('headline'); ?></h3>
							<p><?php the_sub_field('deck'); ?></p>
						</div>
					
					<?php endwhile; endif; ?>
					
					<?php if(get_sub_field('cta_mail')): ?>
						<a href="mailto:<?php the_sub_field('cta_mail'); ?>?subject=<?php the_sub_field('cta_label'); ?>" class="cta"><?php the_sub_field('cta_label'); ?></a>
					<?php endif; ?>
					<?php if(get_sub_field('cta_link')): ?>
						<a href="<?php the_sub_field('cta_link'); ?>" class="cta"><?php the_sub_field('cta_label'); ?></a>		
					<?php endif; ?>
					
				</div>	
			</section>
			
	    <?php endif; ?>
	 
	<?php endwhile; endif; ?>


<?php get_footer(); ?>