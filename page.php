<?php get_header(); ?>

	<section id="default-page">
		<div class="wrapper">
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
				<div class="header">
					<h1><?php the_title(); ?></h1>
				</div>
	
				<div class="body">
					<?php the_content(); ?>
				</div>
							
			<?php endwhile; endif; ?>
			
		</div>
	</section>

	<?php include('inc/contact.php'); ?>

<?php get_footer(); ?>