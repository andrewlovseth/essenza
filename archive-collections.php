<?php get_header(); ?>

	<section id="page-header">
		<div class="wrapper">
			<div class="info">
				<h3><?php the_field('collections_headline', 'options'); ?></h3>
				<p><?php the_field('collections_deck', 'options'); ?></p>
			</div>
		</div>
	</section>

	<section id="list">
		
			<div class="col">
			
				<h3>Fragrance</h3>
	            <?php the_collection('fragrance'); ?>

				<h3>Sleep</h3>
				<?php the_collection('sleep'); ?>
							
			</div>

			<div class="col">

				<h3>Jewelry</h3>
				<?php the_collection('jewelry'); ?>


				<h3>Baby</h3>
				<?php the_collection('children'); ?>
							
			</div>		

			<div class="col">			

				<h3>Bath & Body</h3>
				<?php the_collection('bath-and-body'); ?>
				
				<h3>Skincare</h3>
				<?php the_collection('skincare'); ?>
								
			</div>

			<div class="col">
			
				<h3>Home</h3>
				<?php the_collection('home'); ?>

				<h3>Mens</h3>
				<?php the_collection('mens'); ?>

				<h3>Gifts</h3>
				<?php the_collection('gifts'); ?>

			</div>

	</section>

	<?php get_template_part('partials/contact'); ?>

<?php get_footer(); ?>