<?php get_header(); ?>

	<?php if(have_rows('shop')): ?>
	
		<section id="types">
		
		<?php while(have_rows('shop')) : the_row(); ?>
		 
		    <?php if( get_row_layout() == 'type' ): ?>
				
				<section class="type <?php echo sanitize_title_with_dashes(get_sub_field('cta_label')); ?>">
					<div class="header">
						<h2><?php the_sub_field('headline'); ?></h2>
					</div>

					<div class="photo cover" style="background-image: url(<?php $image = get_sub_field('photo'); echo $image['url']; ?>)">
						<?php if(get_sub_field('cta_link')): ?>
							<a href="<?php the_field('shopify_url' ,'options'); ?><?php the_sub_field('cta_link'); ?>" class="cta"><span><?php the_sub_field('cta_label'); ?></span></a>
						<?php endif; ?>
						<div class="overlay"></div>
					</div>
				</section>
				
		    <?php endif; ?>
		 
		<?php endwhile; ?>
		
		</section>
	
	<?php endif; ?>

	<?php get_template_part('partials/contact'); ?>

<?php get_footer(); ?>