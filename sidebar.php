<div id="sidebar">

	<div id="recent-list">
		<h4>Recent Posts</h4>

		<?php 
			$args = array( 'post_type' => 'post', 'posts_per_page' => 5 );
			$wp_query = new WP_Query( $args );
			if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
			
			<div class="post">
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<h5>Posted on <?php the_time('F j, Y'); ?></h5>
			</div>
			
			    
		<?php endwhile; endif; wp_reset_query(); ?>	
		
	</div>

	<div id="archives-list">
		<h4>Archives</h4>
		<ul>
			<?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12, 'after' => '<span>/</span>' ) ); ?>
		</ul>
	</div>

	<div id="tags-list">
		<h4>Tags</h4>
		
		<?php all_tags(); ?>
	</div>



</div>