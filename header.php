<!DOCTYPE html>
<html class="no-js">
	<head>
		<meta name="google-site-verification" content="wL3WNrcxC_IchHii0AYxl9yRqEkP957PIbKKHxWtGds" />
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>

		<meta name="viewport" content="width=device-width, initial-scale=1">

		<script type="text/javascript" src="//use.typekit.net/epw7ebk.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />

		<script src="<?php bloginfo('template_directory') ?>/js/modernizr-2.7.1.min.js"></script>

		<?php wp_head(); ?>	
	</head>

	<body <?php body_class(); ?> id="<?php echo the_slug();?>">

		<header>		
		
		  <div id="logo">
		      <a href="<?php echo site_url('/'); ?>">
		          <img src="<?php bloginfo('template_directory') ?>/img/logo.png" alt="Essenza" />
		      </a>
		  </div>
		  
		  <nav id="main">
		      <ul>
		          <li><a href="<?php echo site_url('/info/'); ?>">Info</a></li>
		          <li><a href="<?php echo site_url('/services/'); ?>">Services</a></li>
		          <li><a href="<?php echo site_url('/shop/'); ?>" class="shop-nav">Shop</a></li>
		          <li><a href="<?php echo site_url('/collections/'); ?>">Collections</a></li>
		      </ul>
		  </nav>
		  
		  <div id="utilities">
		      
		      <div class="social-links">
		          <ul>
		              <li><a href="<?php the_field('instagram', 'options'); ?>" class="ir instagram" rel="external">Instagram</a></li>
		              <li><a href="<?php the_field('facebook', 'options'); ?>" class="ir facebook" rel="external">Facebook</a></li>
		              <li><a href="<?php the_field('pinterest', 'options'); ?>" class="ir pinterest" rel="external">Pinterest</a></li>
		          </ul>			
		      </div>
		      		      
		  </div>
		      
		</header>
		
		<?php get_template_part('partials/shop-nav'); ?>
