<?php get_header(); ?>

	<section id="page-header" class="cover" style="background-image: url(<?php $image = get_field('hero'); echo $image['url']; ?>)">
		<div class="wrapper">
			<h3><?php the_field('headline'); ?></h3>
			<p><?php the_field('deck'); ?></p>
		</div>
	</section>

	<?php if(have_rows('features')): ?>
	
		<section id="features">
		
		<?php while(have_rows('features')) : the_row(); ?>
		 
		    <?php if( get_row_layout() == 'feature' ): ?>
				
			<section class="feature">
			
				<div class="photo cover" style="background-image: url(<?php $image = get_sub_field('photo'); echo $image['url']; ?>)">
					<div class="overlay">&nbsp;</div>
					&nbsp;
				</div>
				
				<div class="info">
					<h3><?php the_sub_field('headline'); ?></h3>
					<p><?php the_sub_field('deck'); ?></p>
					<a href="<?php the_sub_field('cta_link'); ?>" class="cta"><span><?php the_sub_field('cta_label'); ?></span></a>
				</div>
				
			</section>
				
		    <?php endif; ?>
		 
		<?php endwhile; ?>
		
		</section>
	
	<?php endif; ?>

	<?php get_template_part('partials/contact'); ?>
	
<?php get_footer(); ?>