<?php get_header(); ?>

	<section id="collection-header">
		<div class="wrapper">
			
			<div class="info">
				<?php the_field('info'); ?>		
			</div>
			
		</div>
	</section>


	<section id="contact-info" class="blue">
		<div class="wrapper">
			
			<h2>Locations & Hours</h2>
			<p><?php the_field('address', 'options'); ?></p>
			<p><?php the_field('store_hours', 'options'); ?></p>

			<h2 id="questions">Question & Inquiries</h2>   
			<p>
				<?php the_field('phone', 'options'); ?><br/>
			    <a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a>
			</p> 
			                       
		</div>
	</section>

	<section id="map">
		<div class="wrapper">
		
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2687.7461807970562!2d-122.35043800000003!3d47.65049899999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5490150739fe938d%3A0x6130d2ea7840c842!2sEssenza+Inc!5e0!3m2!1sen!2sus!4v1415826512735" width="100%" height="450" frameborder="0" style="border:0"></iframe>
		
		</div>
	</section>

<?php get_footer(); ?>