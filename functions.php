<?php

// Theme Support
add_theme_support( 'post-thumbnails' );

// Filters
add_filter('the_content', 'filter_ptags_on_images');

function the_slug() {
	global $post;
	$post_data = get_post($post->ID, ARRAY_A);
	$slug = $post_data['post_name'];
	return $slug;
}

function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

function the_collection($cat) {

	$args = array( 'post_type' => 'collections', 'orderby' => 'title', 'order' => 'ASC', 'category_name' => $cat, 'posts_per_page' => 200 );
	$wp_query = new WP_Query( $args );
	
	$output .= '<div class="list-wrapper">';
	
	if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
	
			$permalink = get_permalink();
			$title = get_the_title();
	
	    	$output .= '<a href="' . $permalink . '">' . $title . '</a>';
	    
	endwhile; endif; wp_reset_query();
	
	$output .= '</div>';
	
	
	echo $output;

}


show_admin_bar(false);

function custom_excerpt_length( $length ) {
	return 26;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );



function all_tags() {
	$tags = get_tags();
	foreach ( $tags as $tag ) {
		$tag_link = get_tag_link( $tag->term_id );
			
		$html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
		$html .= "{$tag->name}</a>, ";
	}
	echo $html;
}



if(function_exists('acf_add_options_page')) {
    acf_add_options_page();
    acf_add_options_sub_page('Header');
    acf_add_options_sub_page('Footer');
}
