<?php get_header(); ?>

	<section id="collection-header">
		<div class="wrapper">
			
			<div class="info">
				<h1>Archive: <?php single_month_title(' '); ?></h1>
			</div>
			
		</div>
	</section>

	<section id="blog-archive">
		<div class="wrapper">
		
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
					<article>
						
						<div class="photo">
							<a href="<?php the_permalink(); ?>"><img src="<?php $image = get_field('featured_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
						</div>
						
						<div class="info">
							<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

							<h4><?php the_time('F j, Y'); ?></h4>
							<h5>Posted by: <?php the_author(); ?></h5>
		
							<?php the_excerpt(); ?>
		
							<a href="<?php the_permalink(); ?>" class="read-more">Read more</a>
						</div>
						
					</article>
					
				<?php endwhile; ?>
		
			<?php endif; ?>
		
		</div>
	</section>

	<?php get_template_part('partials/contact'); ?>

<?php get_footer(); ?>