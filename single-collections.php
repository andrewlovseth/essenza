<?php get_header(); ?>

	<section id="leaf-pattern"> </section>
	
	<section id="breadcrumbs">
		<a href="<?php echo site_url('/collections/'); ?>">&lt; Back to all designers & collections</a>
	</section>
	
	<article>
		<div class="wrapper">
			<h1><?php the_title(); ?></h1>
			<h2><?php the_field('location'); ?></h2>
			
			<div id="description">
				<?php the_field('description'); ?>
			</div>
			
			<div id="additional-info">
				
				<div class="categories">
					<h3>Product Categories</h3>
					<p>
						<?php
							$categories = get_the_category();
							$separator = ' / ';
							$output = '';
							if($categories){
								foreach($categories as $category) { $output .= $category->cat_name . $separator; }
								echo trim($output, $separator);
							}
						?>
					</p>
					
				</div>
				
				<?php if(get_field('in_shop')): ?>
					<a href="<?php the_field('shopify_url', 'options'); ?>collections/<?php echo the_slug(); ?>/" id="shop-link">Shop Products Online</a>
				<?php endif; ?>
				
				<p id="note">Want to know if we carry a certain product in this designer/collection in store?<br/>
					Send us an email at <a href="mailto:info@essenza-inc.com">info@essenza-inc.com</a></p>
					
			</div>

		</div>
	</article>


	<?php if(get_field('hero_photo')): ?>
		<section class="photo cover" style="background-image: url(<?php $image = get_field('hero_photo'); echo $image['url']; ?>)">
			&nbsp;
		</section>
	<?php endif; ?>
	
	<?php get_template_part('partials/contact'); ?>

<?php get_footer(); ?>