			
		<footer>
		  <div class="wrapper">
		  
		  
		      <div class="col" id="contact-details">
		          <h4>Contact</h4>
		          <?php the_field('contact_details', 'options'); ?>
		      </div>
		      
		
		      <div class="col" id="store-hours">
		          <h4>Store Hours</h4>
				  <?php the_field('store_hours', 'options'); ?>
		      </div>

		      <div class="col" id="social">
		          <h4>Follow Us</h4>
		      
		          <h5 class="facebook">
		              <a href="<?php the_field('facebook', 'options'); ?>" rel="external">
		                  <span class="network">Facebook</span>
		                  <span class="url">/essenzaseattle</span>
		              </a>
		          </h5>
		
		          <h5 class="instagram">
		              <a href="<?php the_field('instagram', 'options'); ?>" rel="external">
		                  <span class="network">Instagram</span>
		                  <span class="url">@essenzaseattle</span>
		              </a>
		          </h5>					
		          
		          <h5 class="pinterest">
		              <a href="<?php the_field('pinterest', 'options'); ?>" rel="external">
		                  <span class="network">Pinterest</span>
		                  <span class="url">essenzaseattle</span>
		              </a>
		          </h5>	
		      </div>
		      
		
		      <div class="col" id="sister-stores">
		          <h4>Sister Store</h4>
		      
		          <a target="_blank" href="<?php the_field('les_amis_url', 'options'); ?>"><img style="width:120px;" src="<?php $image = get_field('les_amis_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>				
		      </div>
		
			  <div id="sign-up">
				  <a href="http://eepurl.com/dbZH41" class="visit-blog">Join Our Mailing List</a>
			  </div>
            
		
		
		      <div id="terms">
			      

			      
		          <a href="<?php echo site_url('/terms-and-conditions/'); ?>">Terms &amp; Conditions</a>
		      </div>
		
		  </div>
		</footer>  

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
		<script src="<?php bloginfo('template_directory') ?>/js/jquery.backstretch.min.js"></script>
		<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>

		<?php wp_footer(); ?>


		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-57000829-1', 'auto');
			ga('send', 'pageview');
			ga('require', 'displayfeatures');
			ga('require', 'linkid', 'linkid.js');
		</script>

	</body>
</html>