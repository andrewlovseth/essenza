
$(document).ready(function() {
	
	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});	

	
	$('.backstretch').each(function() {
		var bgImage = $(this).attr('data-img-src');
		
		if(bgImage) { 
			$(this).backstretch(bgImage);
		}		
	});
	
	$('#home #features section').hover(function(){
		
		$(this).find('.overlay').fadeOut(400);
		
		
	}, function(){
		
		$(this).find('.overlay').fadeIn(400);
		
	});


	$('#shop #types section').hover(function(){
		
		$(this).find('.overlay').fadeOut(400);
		
		
	}, function(){
		
		$(this).find('.overlay').fadeIn(400);
		
	});
	
	

    $('.wp-caption').removeAttr('style');
    $('.wp-caption img').removeAttr('width').removeAttr('height');

	$('#features > section:nth-child(odd)').addClass('even');

	$('#features > section:nth-child(3n+1)').addClass('blue');
	$('#features > section:nth-child(3n+2)').addClass('white');
	$('#features > section:nth-child(3n+3)').addClass('gold');

	$('#services > .services-info:nth-child(3n+1)').addClass('white');
	$('#services > .services-info:nth-child(3n+2)').addClass('blue');
	$('#services > .services-info:nth-child(3n+3)').addClass('gold');


	$('#shop #types > .type:nth-child(4n+1)').addClass('gold');
	$('#shop #types > .type:nth-child(4n+2)').addClass('blue');
	$('#shop #types > .type:nth-child(4n+3)').addClass('white');
	$('#shop #types > .type:nth-child(4n+4)').addClass('gray');

	
	/*
	$('#main .shop-nav').hover(function() {
        $('#shop-nav').slideDown('fast');
    }, function(){
    });


	for (i = 0; i < 8; i++) {
		$('#category .product-list .product:first').clone().insertAfter('#category .product-list .product:last');
	}

	for (i = 0; i < 2; i++) {
		$('#product .product-list .product:first').clone().insertAfter('#product .product-list .product:last');
	}
	    */

    productDimensions();
    $(window).on("resize", productDimensions);

    homePhotoOverlay();
    $(window).on("resize", homePhotoOverlay);
	
    shopPhotoOverlay();
    $(window).on("resize", shopPhotoOverlay);	
	
	
});


function productDimensions() {
 
	var productImageWidth = $('.product .photo-link img').width(),
		productImageHeight = $('.product .photo-link img').height();
	
	$('.product .overlay').width(productImageWidth).height(productImageHeight);


	var productViewWidth = $('.product .overlay span').innerWidth(),
		productViewHeight = $('.product .overlay span').innerHeight(),
		productViewLeftMargin = (productImageWidth - productViewWidth) / 2, 
		productViewTopMargin = (productImageHeight - productViewHeight) / 2;
	
	$('.product .overlay span').css({ left : productViewLeftMargin, top : productViewTopMargin });
 
}


function homePhotoOverlay() {
 
 	$('#home .photo').each( function() {

		var homePhotoImageWidth = $(this).width(),
			homePhotoImageHeight = $(this).height();

		$(this).children('.overlay').width(homePhotoImageWidth).height(homePhotoImageHeight);

 	});
 
}


function shopPhotoOverlay() {
 
 	$('#shop .photo').each( function() {

		var shopPhotoImageWidth = $(this).width(),
			shopPhotoImageHeight = $(this).height();

		$(this).children('.overlay').width(shopPhotoImageWidth).height(shopPhotoImageHeight);

 	});
 
}



// On Window Resize
$(window).on("resize", function () {


    
    // If Window is greater than or equal to 640px
    if($(window).width() >=640) {


		$('#shop-nav').removeAttr('style');	    

	        
    // If Window is less than or equal to 639px    
    } else {
    


    }	

});