<section id="contact">
	<div class="wrapper">
	
		<h3>Visit our store</h3>
		
		<div id="contact-info">
			<span class="address"><?php the_field('address', 'options'); ?></span> &middot; 
			<span class="phone"><?php the_field('phone', 'options'); ?></span> &middot; 
			<span class="google-maps"><a href="<?php the_field('google_maps_link', 'options'); ?>" rel="external">google maps</a></span>
		</div>
		
	</div>
</section>

<div id="contact-separator">
	&nbsp;
</div>