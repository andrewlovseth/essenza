<nav id="shop-nav">

    <ul>
      	<li><a href="https://shop.essenza-inc.com/collections/types?q=Jewelry">Jewelry</a></li>
      	<li><a href="https://shop.essenza-inc.com/collections/types?q=Apothecary">Apothecary</a></li>
        <li><a href="https://shop.essenza-inc.com/collections/types?q=Fragrance">Fragrance</a></li>
        <li><a href="https://shop.essenza-inc.com/collections/types?q=Home+Goods">Home Goods</a></li>
        <li><a href="https://shop.essenza-inc.com/collections/types?q=Baby">Baby</a></li>
        <li><a href="https://shop.essenza-inc.com/collections/types?q=Gift+Sets">Gift Sets</a></li>
        <li><a href="https://shop.essenza-inc.com/products/gift-certificate">Gift Certificates</a></li>
    </ul>			
    
</nav>