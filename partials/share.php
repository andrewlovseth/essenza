<?php $postid = get_the_ID(); ?>

<div class="share">
	<ul>
		<li><a href="http://www.facebook.com/sharer.php?s=100
			&p[url]=<?php the_permalink(); ?>?>
			&p[images][0]=<?php $image = get_field('featured_image', $postid); echo $image['sizes']['large']; ?>
			&p[title]=<?php echo get_the_title(); ?> | Essenza
			&p[summary]=<?php echo get_the_excerpt(); ?>" class="ir facebook">Facebook</a></li>
	</ul>
</div>