<?php get_header(); ?>


	<section class="post">
		<div class="wrapper">

			<section id="breadcrumbs">
				<a href="<?php echo site_url('/blog/'); ?>">&lt; Back to all posts</a>
			</section>
		
			<?php while ( have_posts() ) : the_post(); ?>
			
				<article>

					<div class="featured-photo">
						<a href="<?php the_permalink(); ?>"><img src="<?php $image = get_field('featured_image'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
					</div>
					
					<h1><?php the_title(); ?></h1>
					<h4>Posted by: <?php the_author(); ?> on <?php the_time('F j, Y'); ?></h4>
					<?php the_content(); ?>
					
					
					<div class="post-footer">
							<?php get_template_part('partials/share'); ?>
						
						<div class="post-tags">
							<?php the_tags('', ', ', '' ); ?> 
						</div>
					</div>
					
				</article>
			
			<?php endwhile; ?>
		
			<?php get_sidebar(); ?>
		</div>
	</section>
	
	<?php get_template_part('partials/contact'); ?>

<?php get_footer(); ?>